from pandas import DataFrame
from typing import Tuple


def separate_to_num_cat(
        train_data: DataFrame,
        cat_columns: list,
        num_columns: list
) -> Tuple[DataFrame, DataFrame]:
    """
    Функция для разделения данных на числовые и категориальные.
    :param train_data: Данные для разделения
    :param cat_columns: Категориальные колонки
    :param num_columns: Числовые колонки
    :return: Пара числовых и категориальных данных (numeric, category)
    """
    df_num = train_data[cat_columns]
    df_cat = train_data[num_columns]
    return df_num, df_cat
