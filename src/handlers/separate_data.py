import click
import pandas as pd
import os


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def main(input_path: str, output_path: str):
    """
    Функция для разделения данных на числовые и категориальные.
    :param output_path: путь до каталога, в который нужно
    сохранить категориальные и числывое данные
    :param input_path: путь до файла с сырыми данными
    :return:
    """
    train_data = pd.read_csv(input_path)
    dirname = "/".join(output_path.split("/")[:-1])
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    if "df_num" in output_path:
        df_num = train_data[["PassengerId", "Survived", "Pclass",
                             "Age", "SibSp", "Parch", "Fare"]]
        df_num.to_csv(f"{output_path}")
    if "df_cat" in output_path:
        df_cat = train_data[["Name", "Sex", "Ticket", "Cabin", "Embarked"]]
        df_cat.to_csv(f"{output_path}")


if __name__ == "__main__":
    main()
