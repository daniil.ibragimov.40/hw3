import pandas as pd
from pandas import DataFrame
from typing import Tuple


def get_train_test_data(
        path: str,
        train_file_name: str,
        test_file_name: str
) -> Tuple[DataFrame, DataFrame]:
    """
    Функция для считывания данных в DataFrame.
    Необходимос указать путь до каталога и имена файлов
    с тестовыми и тренировочными данными.
    :param test_file_name: Имя файла с тестовыми данными
    :param train_file_name: Имя файла с тренировочными данными
    :param path: Путь до каталога
    :return: Пара тренировочных и тестовых данных (train, test)
    """
    train_data = pd.read_csv(f"{path}/{train_file_name}")
    test_data = pd.read_csv(f"{path}/{test_file_name}")
    return train_data, test_data
